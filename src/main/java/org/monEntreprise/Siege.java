package org.monEntreprise;

public class Siege {
    private String numSiege;
    private Billet billet;

    public Siege(String numSiege) {
        this.numSiege = numSiege;
    }

    public String getNumSiege() {return numSiege;}

    public Billet getBillet() {
        return billet;
    }

    public void setNumSiege(String numSiege) {this.numSiege = numSiege;}

    public void setBillet(Billet billet) {
        this.billet = billet;
    }
}
