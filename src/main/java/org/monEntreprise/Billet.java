package org.monEntreprise;

public class Billet {
    private Integer prix;
    private Siege siege;
    private Vol vol;
    private Passager passager;

    public Billet(Vol vol) {
        this.vol = vol;
    }

    public Integer getPrix() {
        return prix;
    }

    public Siege getSiege() {
        return siege;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public void setSiege(Siege siege) {
        this.siege = siege;
    }

    public Vol getVol() {return vol;}

    public Passager getPassager() {return passager;}

    public void setPassager(Passager passager) {this.passager = passager;}

    public void setVol(Vol vol) {this.vol = vol;
    }
}
