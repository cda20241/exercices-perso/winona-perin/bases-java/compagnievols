package org.monEntreprise;

import java.util.Date;
import java.util.List;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Compagnie compagnie1 = new Compagnie("A");
        Compagnie compagnie2 = new Compagnie("B");

        Avion avion1 = new Avion("Airbus");

        compagnie1.creerVol("Berlin", "Tokyo",
                new Date(2023, 10, 17),
                new Date(2023, 10, 18), avion1);
        compagnie1.creerVol("Paris", "Tokyo",
                new Date(2023, 10, 17),
                new Date(2023, 10, 18), new Avion("Mirage"));


        List<Vol> volsParis = compagnie1.getVolsByVilleDepart("Paris");
        for (Vol vol : volsParis) {
            compagnie1.annulerVol(vol);
        }

        //___NOUVEAU_VOL___//

        Vol volBastia = new Vol("Nantes", "Bastia",
                new Date(2023, 12, 17),
                new Date(2024, 01, 12));

        compagnie1.creerVol(volBastia);                         //on créer un nouveau vol

        volBastia.setAvion(avion1);                             //on attibue un vol à cet avion

        Passager wino = new Passager("Winona");            // on créer un passager         // commande un billet

        Siege siegeChoisi = new Siege("A1");           // on choisi le billet
        Billet billet = wino.acheterBillet(volBastia, siegeChoisi);
        avion1.retirerSiege(siegeChoisi);                    // on supprime le billet choisi de la liste




    }
}