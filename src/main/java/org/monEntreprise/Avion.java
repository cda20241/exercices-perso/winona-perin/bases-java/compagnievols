package org.monEntreprise;

import java.util.ArrayList;
import java.util.List;

public class Avion {
    private String modele;
    private List<Siege> sieges;

    public Avion(String modele) {
        this.modele = modele;

        this.sieges = new ArrayList<>();

        for (char lettre = 'A'; lettre <= 'I'; lettre ++) {
            for (int chiffre = 1; chiffre <= 20; chiffre++) {
                String numSiege = lettre + Integer.toString(chiffre);
                Siege siege = new Siege(numSiege);
                sieges.add(siege);
            }
        }
    }

    public String getModele() {
        return modele;
    }

    public List<Siege> getSieges() {
        return sieges;
    }

    public void setSieges(List<Siege> sieges) {this.sieges = sieges;}

    public void retirerSiege(Siege siege) {
        List<Siege> siegesDisponibles = this.getSieges();
        siegesDisponibles.remove(siege);
    }
}
