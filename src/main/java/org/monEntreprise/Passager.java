package org.monEntreprise;

public class Passager {
    private String nom;

    public Passager(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    //___methods___//

    public Billet acheterBillet(Vol vol, Siege siege) {
        Billet billet = new Billet(vol);
        billet.setVol(vol);
        billet.setSiege(siege);
        billet.setPassager(this);

        return billet;
    }
}
